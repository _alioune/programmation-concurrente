package org.alioune.progConc.Exo2;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class WareHouse {
	private int capacity;
	private LinkedList<Integer> caisse=new LinkedList<Integer>();
	private int[] buffer;
	private int index=0;
	public int getCapacity() {
		return capacity;
	}
	public WareHouse(int capacity) {
		this.capacity=capacity;
	//	buffer=new int[capacity];
	}
	public boolean add(int value) {
		if(caisse.size()==capacity)
			return false;
		else
			caisse.add(value);
		return true;
	}
	public boolean remove() {
		if(caisse.isEmpty())
			return false;
		else
			caisse.remove(caisse.size()-1);
		return true;
	}
	public int content() {
		return caisse.size();
	}
	 public void produce() throws InterruptedException 
     { 
         int value = 0; 
             synchronized (this) 
             { 
                 // producer thread waits while list 
                 // is full 
                 while (caisse.size()==capacity) 
                     wait(); 

                 System.out.println("Producer produced-"
                                               + value); 

                 // to insert the jobs in the list 
                 caisse.add(value++); 

                 // notifies the consumer thread that 
                 // now it can start consuming 
                 notifyAll(); 

                 // makes the working of program easier 
                 // to  understand 
                 Thread.sleep(100); 
             } 
     } 
	// Function called by consumer thread 
     public void consume() throws InterruptedException 
     { 
             synchronized (this) 
             { 
                 // consumer thread waits while list 
                 // is empty 
                 while (caisse.size()==0) 
                     wait(); 

                 //to retrive the ifrst job in the list 
                 int val = caisse.removeFirst(); 

                 System.out.println("Consumer consumed-"
                                                 + val); 

                 // Wake up producer thread 
                 notify(); 

                 // and sleep 
                 Thread.sleep(100); 
             } 
         } 

}
