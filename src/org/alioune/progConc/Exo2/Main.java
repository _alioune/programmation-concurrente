package org.alioune.progConc.Exo2;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {
	public static void main(String...args) throws InterruptedException {
		WareHouse entrepot=new WareHouse(15);
		Runnable add=()->
			            entrepot.add(1);
		Runnable remove=()->
		                   entrepot.remove();
	    	
		add.run();
		add.run();
		add.run();
		System.out.println(entrepot.content());
		remove.run();
		System.out.println(entrepot.content());
		// cr�ation d'un service d'ex�cution, dot� de 5 thread
		ExecutorService service = Executors.newFixedThreadPool(5);
		WareHouse entrepot2=new WareHouse(10);
		Runnable add2=()-> entrepot2.add(1);
          Runnable remove2=()->entrepot2.remove();
		
		for(int i=0;i<100;i++) {
			service.submit(add2);
		}
		for(int i=0;i<95;i++) {
			service.submit(remove2);
		}
		System.out.println("--------------------------------");
		System.out.println(entrepot2.content()); //race concurrence 
		//pour r�gler �a
		System.out.println("--------------------------------");
		WareHouse entrepot3=new WareHouse(6);
		Runnable consume=()->{
			try {
				entrepot3.consume();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		};
		Runnable produce=()->{
			try {
				entrepot3.produce();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		};
		for(int i=0;i<10;i++) {
			service.submit(produce);
		}
		for(int i=0;i<5;i++) {
			service.submit(consume);
		}
		service.shutdown();
		service.awaitTermination(1000, TimeUnit.MILLISECONDS);
		Thread.sleep(3000);
		System.out.println(entrepot2.content());
		

	}

}
