package org.alioune.progConc.Exo3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.Provider.Service;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {

    public static void main(String[] args) throws IOException {

        ServerSocket server = new ServerSocket(8080);
        ExecutorService service = Executors.newFixedThreadPool(2);
        Map<String, Integer> AVendre=new HashMap<String, Integer>();
        
        while (true) {
            System.out.println("Listening to request");
            Socket socket = server.accept();
            Runnable task=()->{
            	System.out.println("Accepting request");

                InputStream inputStream = null;
				try {
					inputStream = socket.getInputStream();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

                OutputStream outputStream = null;
				try {
					outputStream = socket.getOutputStream();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
                PrintWriter writer = new PrintWriter(new OutputStreamWriter(outputStream));

                String order = null;
				try {
					order = reader.readLine();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
                while (order != null) {
                    if (order.startsWith("GET")) {
                        writer.printf("Received GET order : %s\n", order);
                        writer.flush();
                        System.out.printf("Received GET order : %s\n", order);
                    }else if(order.startsWith("PUT")) {
                    	writer.printf("Received GET order : %s\n", order);
                        writer.flush();
                    	AVendre.put(order.split(" ")[1], Integer.parseInt(order.split(" ")[2]));
                    }else if(order.startsWith("LIST")) {
                        String chaine="";
                        StringBuilder sb=new StringBuilder();
                        sb.append("debut");
                    	AVendre.forEach((key, value) -> {
                    		String ch="nom Objet : " + key + " Prix : " + value;
                    		sb.append(ch);
                    		sb.append("\n");
                    	});
                    	sb.append("fin");
                    	chaine=sb.toString();
                    	writer.println(chaine);
                    	writer.flush();
                    }
                  /*  else if(order.startsWith("BUY")) {
                    	AVendre.
                    }*/
                    else if (order.equals("bye")) {
                        System.out.printf("Closing connection\n");
                        try {
							socket.close();
							break;
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
                    }
                    try {
						order = reader.readLine();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                }

            };
          
            service.submit(task);
        }
    }
}
