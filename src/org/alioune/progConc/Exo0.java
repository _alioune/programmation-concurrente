package org.alioune.progConc;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Exo0 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Thread t=Thread.currentThread();
		System.out.println(t.getName());
		
		Runnable task= ()->{
			Thread thread=Thread.currentThread();
			System.out.println(thread.getName());
		};
		Thread thread2=new Thread(task);
		thread2.start(); // le nom Thread-0
		 // cr�ation d'un service d'ex�cution, dot� de 1 thread
		ExecutorService service = Executors.newFixedThreadPool(1);
		service.submit(task); // pool-1-thread-1
		

	}

}
