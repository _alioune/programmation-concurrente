package org.alioune.progConc;

import java.util.ArrayList;
import java.util.List;

public class Compteur {
	private int compteur;

	public int getCompteur() {
		return compteur;
	}

	public void setCompteur(int compteur) {
		this.compteur = compteur;
	}
	public void increment() {
		Object lock=new Object();
		synchronized(lock) {
			compteur++;
		}
		
	}
	public static void main(String... args) {
		Compteur cmp=new Compteur();
		Runnable task=()->{
			
		for(int i=0;i<100;i++) {
				cmp.increment();
			}	
		};
		Thread t1=new Thread(task);
		t1.start(); //il y'a bien 100
		try {
			t1.join();
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(cmp.getCompteur());
		cmp.setCompteur(0);
		List<Thread> threads=new ArrayList<Thread>();
		for(int i=0;i<10;i++) {
			threads.add(i,new Thread(task));
		}
		
			threads.forEach(
				thread->
				{
					thread.start();
					try {
						thread.join();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
		);
		System.out.println(cmp.getCompteur()); //on s'attend � avoir 1000, on observe 985 because race condition
		//solution: synchronized
		
		
	}

}
