package org.alioune.progConc;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class AtomicCompteur {
	private AtomicLong compteur=new AtomicLong();
	public void increment() {
		
		compteur.set(compteur.incrementAndGet());
	}
	

	public AtomicLong getCompteur() {
		return compteur;
	}


	public void setCompteur(long newValue) {
		this.compteur.set(newValue);
	}


	public static void main(String...args) {
		AtomicCompteur atcompt=new AtomicCompteur();
		Runnable task=()->{
			
			for(int i=0;i<100;i++) {
					atcompt.increment();
				}	
			};
		List<Thread> threads=new ArrayList<Thread>();
		for(int i=0;i<10;i++) {
			threads.add(i,new Thread(task));
		}
		threads.forEach(t->{
			t.start();
			try {
				t.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		System.out.println(atcompt.getCompteur().get()); // oui c'est plus simple	
	}

}
